#include <string>

int countCharOnEven(std::string phrase, char letter){
    int count = 0;
    for(int i = 0; i < phrase.length(); i++){
        if(i%2 == 0 && phrase.at(i) == letter){
            count++;
        }
    }
    return count;
}


int computeFibonacci(int number){
    int a = 0;
    int b = 1;
    int temp;
    if(number < 1){
        return a;
    }
    for(int i = 1; i < number; i++){
        temp = a + b;
        a = b;
        b = temp;
    }
    return b;
}